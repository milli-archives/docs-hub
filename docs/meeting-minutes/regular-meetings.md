
# Milli Conversation Minutes notes
**Discord channel - https://discord.com/invite/KzjDXC7VzF**
### Call on October 8th
* Started with general discussion
* Prasoon have exploring Gitbook for documentation and also he is checking that Gitbook is Free software platform or not
* 
### Call on October 1st
* Prasoon have updated his work. He is doing uploading the file things, basic UI for the backend
* Team discussed about covid vaccination
* 

### Call on September 28th
* Venkat and Prasoon have discussed about Anand from Internet Archive
* Prasoon have updated backend work
* Prasoon have explained how kala's working
* Prasoon said we have to create organizational to maintain our work and it should be documented
* Venkat and prasoon have discussed what platform we will take into and go forward for the development(Github, Gitlab)
* 


#### shared link
* https://anatomyof.ai/
* https://gitlab.com/servelots
* https://github.com/milli-consortium/

### Call on September 24th
* Started with general discussion
* Venkat, shafali and prasoon have discussed we have to confirm how to interact with team 
* Dinesh, Prasoon and Venkat have discussed mailman3 mailing list.
* Dinesh will write mail to mailman3 and he discussed estimation with venkat
* Prasoon and dinesh have discussed backup, liability of mailman3
* Passon shared the video which they discussed about Renarrative construction
* 

#### shared link
* https://media.ccc.de/v/1NE5joYD0o-gaf5SqL-1Nw
* https://retention.live/
* https://thenewit.eu/
 

### Call on September 15th
* Started with general discussion
* Team discussed the Time convenient  and how to make all the members should be present in the meeting
* Venkat said will wait for 1week to confirm the timing and how to go for the meeting, etc
* Shatakshi asked Bhanu to will make it more stylish on IAW2021 page  https://www.milli.link/iaw2021/
* Bhanu and Prasoon have updated new schema changes and prasoon said write the shell script and clone the backend code 
* Dinesh and Prasoon have disucussed server log issues and Dinesh will ask Ram on this and come back
* Bhanu confirmed after Deploying and testing will confirm the  Time lining
*  Venkat have updated NCBS huge administration and they need  mailing list for the Milli community and NCBS
*  venkat comment opensource.org  uses mailman for its mailing lists, looks like. 
* Prasoon suggested will put deadline for Mailing list
* 

#### Shared link
* https://dagworld.com/archiving-documentation/
* opensource.org
* https://asynchronous.in/docker-mailman/
 

### Call on September 14th
* prasoon have updated development side
* Shatakshi and Bhanu have discussed IAW2021 website
* 

#### Shared link
* https://youtu.be/A3ytTKZf344
* https://naturemorte.com/
### Call on September 3rd
* Prasoon have updated minimal changes in the API 
* Bhanu have updated timeline for the NCBS server
* Prasoon and Venkat have discussed Acknowledge and object related things
* 

#### Shared link
* https://www.bloomberg.com/graphics/2015-paul-ford-what-is-code/
* https://academic.oup.com/mind/article/LIX/236/433/986238
* https://www.notion.so/Design-Theory-History-Aesthetics-6a352ef2efde4a0ca9c3328f5321b7f0
* https://billowy-origami-702.notion.site/c739ecd43da148d491f40df3a48fdb07?v=b3752a4a8d714b4f9aeffab7773c2325
* 
### Call on August 31th
* started with general discussion
* Bhanu and prasoon have discussed delivery from both front end and back end
* sathakshi have updated Description for the app.milli.link.  She added the description content to miro board.
*  
### Call on August 24th
* started with general discussion
* Team have discussed future  job
* Prasoon working on the Performance of data
* Venkat comment some more big data: 
http://www.iucaa.in/attachments/opportunities_attach/ligodc/202107_LIGO_SCTI_OFFF_BDS.pdf
* Shathakshi have presented first draft of Resource document with team members
* Venkat said will make it as Questions and answer
* Bhanu said this doc will look like a blog
* Shathakshi intend to add Archive content is collabaroting with community, inclusivity
* Venkat recomended shatakshi to rename the Archive heading
* Bhanu shared Kotagiri trip updates
* Micah have some photos of their seed bank (internal circ) 
https://docs.google.com/document/d/1xr9MQF-VbX18B-FZmHOXpD8I1JNsVnYJNEFV0DNW2Ig/edit?usp=sharing
* 
####  shared link
* https://iiif.io/about/
* https://recogito.pelagios.org/help/about
* https://dp.la/about
* https://www.europeana.eu/en/about-us
* https://www.zinnedproject.org/materials/peoples-history-of-the-united-states
* Pics from our trip 
https://photos.app.goo.gl/yy3XyC1BZHtMyLQFA
* https://twitter.com/search?q=exam%20questions&s=09
### Call on August 17th
* started with general discussion
* Venkat said once all come backk we will discuss Shathakshi Research 
* Prasoon and venkat have discussed archival and how to help Venkat colleague from Canada. Venkat explained Milli archive to her
* Prosoon have updated classified and unclassified item
* 

### Call on August 6th
* Started with General Discussion
* Shathakshi have updated milli.link IAW2021 recordings and she asked suggestion for the title  
* Team suggested Chapter(maximum), fragments, time stamp
* Venkat  suggested to  make one nav tab for the Events instead of usign IAW2020 and IAW2021 nav tabs in the milli.link website
* Shatakshi came up with topics Annotation conference. 
* Venkat comment This is such incredible painstaking work, you guys. https://www.milli.link/posts/iaw-2021-day-2/  good stuff, methinks. However, I think a backlink to main page for IAW2021 will be good, so there is compatibility and ways to go back and front from 
https://www.milli.link/iaw2021/
 to specific page, and back to high level.
 * Bhanu replied on menu reordering and breadcrumbs to navigate back noted as issues
 * Dinesh explained the annotaion and effect of the annotation, annotation framework what he is thinking
 * Bhanu, shafali and shathakshi have discussed how to create on Github issue
 * Team have discussed APC conference Lecture by APC given by Murali Shanmugavelan Series of lectures on Decolonising and Anti caste perspectives in Tech+Internet 
 * 
 #### Shared Link
 * https://github.com/janastu/ncbs-modular/issues
 * https://www.milli.link/posts/iaw-2021-day-2/
 * https://www.milli.link/iaw2021/
 * https://github.com/milli-consortium/milli-frontend/issues
 * https://www.apc.org/en/news/challenging-hate-lecture-series-will-focus-decolonising-media-communication-and-technology
 * https://sparkar.facebook.com/ar-studio/
  
### Call on August 3rd
* Started with general discussion
* Bhanu and Shathakshi have discussed  Session notes posts https://www.milli.link/iaw2021/#session-recordings-with-notes
* Shatakshi have discussed National Archive level objects
* Bhanu updated what are the things will come next release 
* Bhanu have updated spam mailing from milli mailing list
* Prasoon suggested pasword manager instead of cookies storage
* Venkat,Bhanu, Shathaksi and shafali have discussed session notes how to put the content
* 

##### Shared Link
* https://projectarcc.org/
* https://www.archives.gov/files/education/lessons/worksheets/
* https://youtu.be/LiLS7U7YIdc
* https://www.newyorker.com/magazine/2006/05/15/the-perfect-mark
* https://longnow.org/clock/
* 
### Call on July 30th
* Venkat, shafali and Shathaksi have discussed how to connect school, teachers with the Archive world
* Bhanu will update milli.link once his local version of jekyll is updated
* Bhanu, Prasoon, Dinesh and Joel had a couple of calls about next-steps for milli-tech

Bringin the call to the larger group, the following issues were identified:
1. there are various groups (tech, archive, historian, design, etc) and when one group's topic is being discussed, the others typically fall silent or zone out.
2. till now the main thing driving development was deadlines from conferences
    * in the absence of those deadlines the momentum stalls.

To address both, it was proposed that a common metaphor be chosen that has nothing to do with the subject matter. For instance, Imagine a restaurant:
* there's a cooking area
    * analogous to Tech
* a service area, where patrons are eating their meal
    * analogous to the App being used
* and an exterior area, where marketing and sales are happening
    * analogous to speaking at conferences, speaking to customers, marketing, sales, etc.

The question on everyone's minds is, "What's next for Milli?"
* for tech, it's not clear when we should clean up the code to make it easier for external contributors
    * this is more true on frontend than backend
* in terms of offering services or engaging with prospective clients it also becomes easier to plan if we know what kind of "restaurant" we're running and what our goals are.
    * running a community kitchen is very different than running a Michelin Star restaraunt, which is different from running a McD's franchise.
* From Venkat's perspective, the next step is to complete the Figma version of the Milli Prototype and then start discussing it with the broader community.

#### Questions for Next Call
* what are our future plans, and individual commitments?
* are we still interested in working on Milli, and in what form?
* what kind of effort estimation is left for the Milli Prototype?
* what else is remainig for Satakshi's story to be implemented?

### Call on July 23th
* Team have discussed story of archive, how is it easy and how it difficult
* Discussed abour Resources for the milli, tools, how to introduce the archive to users, specifically for the students
* Shafali suggested will list out the app, tool latr will look into
* Prasoon have updated development works.
* Shafali will handle the discord 
* Prasoon will handle the Milli documentation
* Dinesh will handle Millman and IAW2021 Recordings

#### Shared link:
* https://list.org/
### Call on July 20th
* Venkat and Dinesh have discussed about milli presentation slides
* Dinesh presented Milli community of archive presentation.
* venkat comment "Milli is also a small effort to connect W3C Web Annotations to archival descriptions. The target of annotations are segments of archival descriptions that define how to find them, ala URLs."
* Bhanu suggested the word Narrtives at fringes for the slide3
* Venkat comment on Do include a note up front: "Some of these slides have been used by some members of the Milli Collective in previous presentations of this on-going work." Audience would have seen someof these slides before.Especially any SNAC people
* 
```
localStorage.setItem('token', "518170ca-4ba2-4163-a38c-84f450a5d534")
```

##### Shared Link
* Milli community of archive presentation https://docs.google.com/presentation/d/1Fgt0vzk4FjQq2oFAlvff5c7gIHLqEPKyUW7TU6_Ht7g/edit?pli=1#slide=id.ge0385c4489_3_416
* token - 518170ca-4ba2-4163-a38c-84f450a5d534
* https://www.youtube.com/watch?v=Unzc731iCUY
### Call on July 16th
* Started with general discussion
* Bhnau comment Btw @⁨Venkat Ncbs⁩ @⁨Satakshi Sinha⁩ once we have the frontend publicly accessible, follow these steps to log yourselves in:
    * open browser and navigate to the URL
    * press Ctrl + Shift + J on Chrome (Ctrl + Shift + K for Firefox) to open the console
    * alternatively press F12 and click on `Console`
    * you'll see a blinking cursor in the console, paste the following command and hit enter. 

```
localStorage.setItem('token', "b016175b-9d90-42a6-9119-d7a0ee78145f")
```
*     This will set a token in your localStorage that the app will read and use when fetching data from the backend.
*    Sathakshi comment on a lot of research scholars are not familiar with it. we are more familiar with subject, keywords, tags
*    User agent in current model implementation doesn't have a way to understand the field in the annotation
https://www.milli.link/posts/iaw-2021-day-7/
* Only request: don't remove content on 
https://www.milli.link/iaw2021/
 page: Just move it, if needed. We should preserve the way it was during the Sessions.
 * Don't have clear answers for inclusivity approach. But in general, one thing: asking visitors to site explicitly what to be done is probably a start. We dont need to say we are doing it. Just saying we want to and want to learn?
Long term, we need language variety, etc
* Catching up slowly.
Regarding cleaning up of Milli Sessions/IAW2020/21. Suggestion:
    * Top Level: 
https://www.milli.link/Sessions
    * Sub levels: 
https://www.milli.link/Sessions/2020/
https://www.milli.link/Sessions/2021/
    * sub levels:
https://www.milli.link/Sessions/2020/Overview
    * (As is calendar of events)
https://www.milli.link/Sessions/2020/Event
(Lazy scroll of day: notes, video embeds, etc)

#### Shared Link 
* https://www.thesaurus.com/browse/annotate

### Call on July 13th
* Started with general discussion
* Bhanu updated his form mork and askinghow need to send the query params
* Bhanu,venkat and shathakshi input field,date,subject, Vocabulary, object, UI for the object page
* Venkat and bhanu have discussed object annotation and where we can see the annotated data
* Bhanu said we have to cleanup the Home page
* Shafali said, she will do check the app.milli.link UI objec,home page
* shafali and bhanu said will do hypothesis for the app.milli.link to adding or proposing the new text or comment
* Prasoon explaining what we are going to annotated and what should be the subject and what should be the  object
* venkat have talked about Record context
* Venkat comment LD4 talk should meet this goal from the abstract, methinks: "Milli is also a small effort to connect W3C Web Annotations to archival descriptions. The target of annotations are segments of archival descriptions that define how to find them, ala URLs. Milli users get to overlay new connections to content and metadata segments and store them in decentralized repositories, share them and use them in their stories."
* Joel,bhanu and dinesh have discussed git issue

##### Shared Link
* LD4 sched seems to be updated with other talks in same session: 
https://ld42021.sched.com/event/jo9q/linking-the-enslaved-at-dogue-run-farm-using-records-in-contexts-ontology-in-islandora-milli-toward-a-community-of-archives
* Records in Context, part 1: 
https://www.ica.org/en/records-in-contexts-ric-a-standard-for-archival-description-presentation-congress-2016
* Records in Context, part 2: 
https://blog-ica.org/2020/06/13/ric-o-converter-an-example-of-practical-application-of-the-ica-records-in-contexts-standard-ica-ric/
* Critique of Records in Context: 
https://interparestrust.org/assets/public/dissemination/interparestrust_commentsonric_final2.pdf
* Registration link for the LD4 conference
 https://ld42021.sched.com/
### Call on July 6th
* Shathakshi, Dinesh and Venkat have discussed  about upcoming talk events
* Shathakshi emailed the Google Doc link forfirst section of the resourse doc. 
* Prasoon, venkat and shathakshi have discussed the Unit Id with Team
* Prasoon comment : The crosswalk for ISAD(G) for the above fields as defined by EAD3 standard are available here: [[ISAD(G)-EAD3 crosswalk](
https://www2.archivists.org/sites/all/files/TagLibrary-VersionEAD3.pdf
)]

```
3.1.1 Reference code:
<agencycode> and <recordid> within <control>; <unitid> with @countrycode and @repositorycode

3.1.3 Title:
<unittitle>

3.2.1 Name of Creator:
<origination>

3.1.3 Date of Creation:
<unitdate>, <unitdatestructured>

3.1.5 Extent of Unit of Description:
<physdesc>, <physdescstructured>

3.1.4 Level of description:
<archdesc> and <c> @level
```
*  shathakshi have asked questioned annotaion from the UI what we are puttng in the annotation concept
*  Prasoon said we are going to add experimental data graph there. He said, for testing the annotation, create a your own vocabulary to test the annotation.
*  


##### Shared Link
* https://www.ichst2021.org/
* https://digitalcollections.ohs.org/371n5278
* localStorage.setItem("token", "82e9a9d6-8204-480c-8529-d04f66d089c0")
* 
### Call on July 2nd
* Shatakshi and Prasoon have discussed Meta data, dates and stories.
* Prasoon have discussed about Vocabulary
* Venkat says The WholeLife Academy folks were interested in just that - network of stories and the vocabulary for that
* Venkat and shathakshi have discussed IAW2021 webpage and youtube time stamp
* 
##### Shared Link
* http://vmis.in/
* http://www.indiastudies.org/
* https://www.ica.org/en/records-in-contexts-ric-a-standard-for-archival-description-presentation-congress-2016
* https://blog-ica.org/2020/06/13/ric-o-converter-an-example-of-practical-application-of-the-ica-records-in-contexts-standard-ica-ric/
* https://www.ica.org/en/records-in-contexts-ric-a-standard-for-archival-description-presentation-congress-2016
* Linked Data conference, July 19-23: 
* https://ld42021.sched.com/
* https://vmis.in/ArceCategories/music_in_context_innercat/94
 could have linkages to PARI's work song database.
### Call on June 29th
* Started with general discussion
* Sharatha from Thasla joined meeting today
* Venkat explained Milli Archive Project to Sharadha
* Shratha explained Thasla project to milli team and she asked how to go forward the archive thing with milli people
* Prasoon and venkat clarifier sharatha doubts
* Sharatha will confirm with Thasla team and she will do writing mail and send to the milli team
* Venkat and Sharatha will discussed with their groups and come back to the discussion
* Shathakshi said Thasla project is interesting and it will become challenging or opportunity to Milli
* Prasoon said we have to come for the financial entity
* Venkat and Prasoon have discussed how Milli can perform whether Profitable organisation and Non-profitable organisation
* 
### Call on june 22nd
* Started with general discussion
* Bhanu was doing the UI part and Joel doing UX part . They are facing some issues. 
* Joel explaining the issues and confirming the data how to send to backend with Prasoon
* Bhanu working on the form and form control. He was looking for the form control in Ant design
* Bhanu, Dinesh and Prasoon wlaked through the https://app.milli.link/search/
* Prasoon going to give search  entity type to Bhanu and Joel
* 

#### Shared Link
* https://www.goodreads.com/book/show/5211.A_Fine_Balance
* https://app.milli.link/search/

### Call on June 15th
* Started with general discussion, covid updates
* Team shared their Milli Conference experience
* Action-Items / Decisions on 15 Jun 2021: 
    * We should discord for dev updates (to create ongoing buzz) - @Joel
    * Decide whether LD4 abstract should be changed to be based primarily on figma/ppt walkthrough - @⁨Bhanu Number⁩ 
    * Continue with product roadmap as-is, but
  * devops needs to be improved so that we have better efficiency when creating software - @Joel

### Call on June 4
* Started with general discussion
* Bhanu deployed the Milli application to Pantato sever http://app.milli.link/search
* Joel and Prasoon have discussed about Filter and Object functionality
* Shatakshi presented conference
https://docs.google.com/presentation/d/10TPJLF1wJVAGtbFLCkMsE4UbFU9Zwa8JWoe2AwjLKn4/edit#slide=id.gde914e01fa_0_0
* Shafali and Shatakshi explained the flow of Presentation which the are preparing
* Prasoon and Venkat have shared their thoughts on presentation
* Duration: We actually have two hours in total. First hour for Milli Tech. Second hour for other archives to talk and discuss with audience.So, you can go slow, and let things unfold.
* every loose end is an intentionally dangling link - as a position to contrast the typical snipping and self protecting preemtive tieing up of loose ends in institutional archives
* ie., we are not surprised that there are many ends loose and we hope they lead to an emergence of "milli" - the openly intermediate space


#### Shared link
* https://docs.google.com/presentation/d/10TPJLF1wJVAGtbFLCkMsE4UbFU9Zwa8JWoe2AwjLKn4/edit#slide=id.gde914e01fa_0_5
* http://app.milli.link/search

### Call on June 1
* Started with General Discussion
* Bhanu and Prasoon have discussed Backend issues happened and prasoon have explained how he fixed
* Prasoon and Dinesh have discussed OpenAPI
* Joel have updated objects and schema work flow
* Joel told will share the UI later
* Bhanu have explained frontend side what are the issues they are facing. Because of that issues UI pages for the application is breaking
* Prasoon have explaining why its delaying 
* Dinesh and Venkat have asked something we can show during conference
* Bhanu, Prasoon and Joel have confirmed what are all the functionality and Ui will complete and we can take forward to Conference
* Venkat have updated Conference updates and how we  are going and what we are expecting
* 


#### Shared link"
* https://github.com/knadh/
* https://api.milli.link/graphql-schema
* check out outline. to annotate and discuss articles uses 
hyp.is eg., https://outline.com/7gSrTe



### Call on May 28th
* Prasoon have presented https://api.milli.link/
User, Comment API
* Bhanu have said will pull the latest code from the back end and deploy the front end code
* Shathaksi have confirmed Registration ongoing for the upcoming events
* Venkat, Dinesh and Bhanu have discussed Recording
* Bhanu and Athithya have discussed Icons width and height.Bhanu asked athithya to look into the icons
* Venkat and Prasoon have discussed Story telling 
