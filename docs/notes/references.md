# Notes and References

Use this file (or other files under the `/docs/notes` directory) to gather notes and references that might be too nascent or not particularly in scope to neccessitate inclusion under the navigable pages under the documentation hub.

These files can still be viewed online but, the link to them would not be visible under the menu.

> Use general archiving principles to organise them!
