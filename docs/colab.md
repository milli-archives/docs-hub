# Collaboration

Milli is a community driven project. "Community" here refers to all stakeholders of the project - the people building it, and the people using it. In this spirit, we encourage you to contribute to this project and submitting additions, edits, feedback to improve this documentation is one of the best places to start!
To contribute to this document, submit a PR for the [Milli Docs Hub](https://gitlab.com/milli-archives/docs-hub) on GitLab or simply send an email to us on `hello at milli.link` with the subject line "Milli Docs Hub".
